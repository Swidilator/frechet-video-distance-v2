# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
from pickle import load as p_load
import os
import numpy as np
from typing import Tuple

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import tensorflow as tf

tf.get_logger().setLevel("ERROR")
tf.autograph.set_verbosity(3)

from models import FVDSuper

# tf.autograph.set_verbosity(3)


def load_full_videos(
    original_video_pickle_path: str, output_video_pickle_path: str
) -> Tuple[np.ndarray, np.ndarray]:
    # Number of videos must be divisible by 16.
    video_divisor = 16
    video_length = 16

    with open(original_video_pickle_path, "rb") as f:
        original_video_raw: np.ndarray = p_load(f)
    with open(output_video_pickle_path, "rb") as f:
        output_video_raw: np.ndarray = p_load(f)

    np.testing.assert_equal(original_video_raw.shape, output_video_raw.shape)

    num_frames: int = original_video_raw.shape[0]
    frames_per_set: int = video_divisor * video_length
    num_sets_from_load: int = num_frames // frames_per_set

    original_video_cut = np.zeros(
        [
            num_sets_from_load,
            video_divisor,
            video_length,
            *original_video_raw.shape[1:],
        ]
    )
    output_video_cut = np.zeros(
        [
            num_sets_from_load,
            video_divisor,
            video_length,
            *original_video_raw.shape[1:],
        ]
    )

    # a = np.split(original_video_raw, len(np.arange(0, frames_per_set, VIDEO_LENGTH)))

    for set_no in range(num_sets_from_load):
        for vid_no, vid_frame in enumerate(np.arange(0, frames_per_set, video_length)):
            start_index: int = vid_frame + (frames_per_set * set_no)
            end_index: int = vid_frame + video_length + (frames_per_set * set_no)

            original_video_cut[
                set_no, vid_frame : (vid_frame + video_length)
            ] = original_video_raw[start_index:end_index]
            output_video_cut[
                set_no, vid_frame : (vid_frame + video_length)
            ] = output_video_raw[start_index:end_index]

    return original_video_cut, output_video_cut


def run_fvd_on_video_arrays(data_folder: str):

    original_video_set, output_video_set = load_full_videos(
        os.path.abspath(f"{data_folder}/original_video_array.pickle"),
        os.path.abspath(f"{data_folder}/output_video_array.pickle"),
    )

    square_size = min(original_video_set.shape[3:5])
    long_index = np.argmax(original_video_set.shape[3:5])
    big_size = max(original_video_set.shape[3:5])
    num_video_sets: int = original_video_set.shape[0]

    fvd_total: np.ndarray = np.zeros((num_video_sets, 3))

    fvd_super = FVDSuper()

    # Take left, centre and right square
    slice_indices = [0, 1, 2]
    slice_indices = [x * ((big_size - square_size) // 2) for x in slice_indices]

    for vid_set in range(num_video_sets):
        for slice_no, slice_point in enumerate(slice_indices):

            point_0 = slice_point * abs(long_index - 1)
            point_1 = slice_point * long_index
            slice_range_0 = slice(point_0, point_0 + square_size)
            slice_range_1 = slice(point_1, point_1 + square_size)

            original_video_square = original_video_set[
                vid_set, :, :, slice_range_0, slice_range_1
            ]
            output__video_square = output_video_set[
                vid_set, :, :, slice_range_0, slice_range_1
            ]

            first_set_of_videos = tf.convert_to_tensor(value=original_video_square)
            second_set_of_videos = tf.convert_to_tensor(value=output__video_square)

            fvd_total[vid_set, slice_no] = fvd_super.calculate_fvd(
                first_set_of_videos, second_set_of_videos
            )

    fvd_average = fvd_total[fvd_total.nonzero()].mean()
    print(fvd_average)
    return float(fvd_average)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Masters model main file")

    root_data_folder: str = "/home/kyle/samba_share/MastersProjectRemote/Models/"
    folder_list: list = sorted(os.listdir(root_data_folder))

    print("Choose_model:")
    for i, val in enumerate(folder_list):
        print(f"\t{i}: {val}")
    chosen_model_index: int = int(input("Selection:"))
    # parser.add_argument("data_folder", action="store")

    # args: dict = vars(parser.parse_args())

    # data_folder_from_args: str = args["data_folder"]
    data_folder = os.path.join(
        root_data_folder, folder_list[chosen_model_index], "video_output"
    )
    print(f"Using folder: {data_folder}")
    run_fvd_on_video_arrays(data_folder)
