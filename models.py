import tensorflow as tf
import tensorflow_gan as tfgan
import tensorflow_hub as hub
from tensorflow.python.keras import Model

tf.keras.backend.set_floatx("float64")


class FVDSuper:
    def __init__(self):
        self.fvd_model: FVDModel = FVDModel()

    @tf.function
    def __calculate_fvd(self, first_video_set, second_video_set):
        return self.fvd_model(first_video_set, second_video_set)

    def calculate_fvd(self, first_video_set, second_video_set):
        out: tf.Tensor = self.__calculate_fvd(first_video_set, second_video_set)
        return out.numpy()


class FVDModel(Model):
    def __init__(self):
        super(FVDModel, self).__init__()
        self.i3d_model = hub.load(
            "https://tfhub.dev/deepmind/i3d-kinetics-400/1"
        ).signatures["default"]

    @tf.function
    def preprocess(self, videos, target_resolution):
        """Runs some preprocessing on the videos for I3D model.

      Args:
        videos: <T>[batch_size, num_frames, height, width, depth] The videos to be
          preprocessed. We don't care about the specific dtype of the videos, it can
          be anything that tf.image.resize_bilinear accepts. Values are expected to
          be in the range 0-255.
        target_resolution: (width, height): target video resolution

      Returns:
        videos: <float32>[batch_size, num_frames, height, width, depth]
      """
        videos_shape = videos.shape.as_list()
        all_frames = tf.reshape(videos, [-1] + videos_shape[-3:])
        resized_videos = tf.image.resize(
            all_frames, size=target_resolution, method=tf.image.ResizeMethod.BILINEAR
        )
        target_shape = [videos_shape[0], -1] + list(target_resolution) + [3]
        output_videos = tf.reshape(resized_videos, target_shape)
        scaled_videos = 2.0 * tf.cast(output_videos, tf.float32) / 255.0 - 1
        return scaled_videos

    @tf.function
    def preprocess_and_embed(self, videos: tf.Tensor):
        processed_videos: tf.Tensor = self.preprocess(videos, (224, 224))

        batch_size = 16
        tf.Assert(
            tf.reduce_max(input_tensor=processed_videos) <= 1.001,
            ["max value in frame is > 1", processed_videos],
        ),
        tf.Assert(
            tf.reduce_min(input_tensor=processed_videos) >= -1.001,
            ["min value in frame is < -1", processed_videos],
        ),
        tf.debugging.assert_equal(
            tf.shape(input=processed_videos)[0],
            batch_size,
            "invalid frame batch size: ",
            summarize=6,
        ),
        return self.i3d_model(processed_videos)["default"]

    def call(self, original_vids, generated_vids):
        original_activations = self.preprocess_and_embed(original_vids)
        generated_activations = self.preprocess_and_embed(generated_vids)
        # print(tf.reduce_min(original_activations))
        # print(tf.reduce_max(original_activations))
        return tfgan.eval.frechet_classifier_distance_from_activations(
            original_activations, generated_activations
        )